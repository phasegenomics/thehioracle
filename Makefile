.PHONY: clean

bin/hioracle: gfa1/gfaview htslib/libhts.a
	cd src && $(MAKE)

gfa1/gfaview:
	cd gfa1 && $(MAKE)

htslib/libhts.a:
	cd htslib && $(MAKE)

clean:
	rm -rf bin/hioracle ; gfa1 && make clean ; cd src && make clean