#include <assert.h>
#include <stdio.h>
#include "../../src/queue.h"

int main(int argc, char *argv[]){

  Queue * q = initQueue(sizeof(int));

  uint32_t tmp_put[13] = {1,2,3,1000,5,6,7,1,9,10,11,12,13};

  int tmp, i = 0;

  for(i = 0; i < 13; i++){
    queuePush(q, (void *)&tmp_put[i]);
    fprintf(stderr, "\n");
  }

  for(i = 0; i < 13; i++){
    queuePop(q,(void*)&tmp);
    fprintf(stderr, "TEST: %s : %i : %i == %i\n", __FILE__, __LINE__, tmp_put[i], tmp);

    assert(tmp == tmp_put[i]);
  }



  return 0;

}
