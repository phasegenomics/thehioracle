#!/usr/bin/perl
use strict;
use warnings;
use Getopt::Long;
use List::MoreUtils qw(uniq);

#-----------------------------------------------------------------------------
#----------------------------------- MAIN ------------------------------------
#-----------------------------------------------------------------------------
my $usage = "

Synopsis:

minimap2 --secondary=no reference_genomes.fa contigs.fasta | sort -k1,1 -k8,8n | chimeric_detection.pl

Description:

Detects chimeric contigs by looking for colinearity within multiple alignment blocks.

Output:

A tsv with a line for each query sequence (if they have an alignment entry).

";

my ($help);
my $opt_success = GetOptions('help'    => \$help,
			      );
die $usage if $help || ! $opt_success;

my @DAT;
my $last = "";

while (<STDIN>) {
    chomp;
    my @line = split /\t/, $_;
    process_lines(\@DAT) if($line[0] ne $last) ;
    push @DAT, $_;
    $last = $line[0];
}

process_lines(\@DAT);
#-----------------------------------------------------------------------------
#-------------------------------- SUBROUTINES --------------------------------
#-----------------------------------------------------------------------------

sub process_lines{
    
    my $data = shift;
    return if(scalar @{$data} == 0);

    my %block;
    my $count = 0;

    foreach my $l (@{$data}){
	$count += 1;
	my @line = split /\t/, $l;
	$block{$count}{'qn'} = $line[0];
	$block{$count}{'ql'} = $line[1];
	$block{$count}{'qs'} = $line[2];
	$block{$count}{'qe'} = $line[3];
	$block{$count}{'st'} = $line[4];
	$block{$count}{'tn'} = $line[5];
	$block{$count}{'ts'} = $line[8];
	$block{$count}{'te'} = $line[9];
	
    }

    

    my @strand; 
    my @target_id;

    #since the target is already sorted we can measure if the query is increasing.
    my $increasing = 1;
    my $decreasing = 1;

    $decreasing = 0 if($count == 1 && $block{1}{'st'} eq '+');
    $increasing = 0 if($count == 1 && $block{1}{'st'} eq '-');

    foreach my $key (keys %block){
	push @strand, $block{$key}{'st'};
	push @target_id, $block{$key}{'tn'};
	
	next if($key == 1);
	
	$increasing = 0 if($block{$key}{'qs'} < $block{$key - 1}{'qe'});
	$decreasing = 0 if($block{$key}{'qs'} > $block{$key - 1}{'qe'});
    }

    
    @strand    = uniq @strand;
    @target_id = uniq @target_id;


    my $chimeric = "clean";

    my @reason = ("none");

    push @reason, "two_target_genomes" if(scalar(@target_id) != 1);
    push @reason, "inversion",         if(scalar(@strand) != 1 && scalar(@reason) == 0);
    push @reason, "not_colinear",     if($increasing == 0 && $decreasing == 0);
    
    if(scalar(@reason) != 1){
	$chimeric = "chimeric";
	shift @reason;
    }
   
    my $orientation = "increasing";
    $orientation = "decreasing" if($increasing == 0);

 
    print STDOUT join "\t", ($block{1}{'qn'}, $block{1}{'ql'}, (join ",", @strand), $chimeric, (join ",", @reason), $orientation);
    print STDOUT "\n";


    undef @{$data};
}

