# TheHiOracle

The Hi Oracle trims an assembly graph (GFA) links using Hi-C data.

# Install

git clone https://zevmanchue@bitbucket.org/phasegenomics/thehioracle.git && cd thehioracle && make

# Install with HTSLib and GFA1 submodules

git clone --recurse-submodules https://bitbucket.org/phasegenomics/thehioracle.git && cd thehioracle && make
