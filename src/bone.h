#ifndef BONE_H
#define BONE_H

#include "khash.h"
#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

//This macro makes a key from two uint32_t
#define makekey(x, y)( x < y ? (((uint64_t)x << 32) | y) : (((uint64_t)y << 32) | x) )

// This macro returns the left 32 bits
#define lkey(x)((uint64_t)x >> 32)

// This macro returns the right 32 bits
#define rkey(x)(((uint64_t)x << 32) >> 32)

/*
The bone structure contains link counts between to sequences including
direction. The array is 3:-- 2:-+ 0:++ 1:+-. The sign depends on if the linkage
information is from the first half or second half of the sequence. Total is a
sum of all the orientation counts. The first half of the sequence is referred
to as +. The last position is the total count.
 */

struct bone{
  uint64_t      key;
  double  counts[5];
};

typedef struct bone bone;

// This is the khash map of the type bone
KHASH_MAP_INIT_INT64(s, bone);

struct seq_info{
	uint8_t * sname;
	uint32_t  len;
  uint32_t  namelen;
};

typedef struct seq_info seq_info;

/**
 * The coffin struct is a wrapper to khash. The key of the hashmap is a uint64
 * which is bit packed with two uint32 values:
 *  n1: the index of the first seqid
 *  n2: the index of the second seqid
 *
 *  n1 and n2 are sorted so that the first seqid is always smaller than the second
 *
 *
 *
 * @param s is the name of the hashmap since it's done by macro
 */
struct coffin{
  uint32_t      n_seqs;
  uint32_t      n_keys;
  seq_info     * sinfo;
  khash_t(s) * hashmap;
};


typedef struct coffin coffin;


/**
 * Creates and allocates memory for the coffin and the contained khash map
 * @return a coffin *
 */
coffin * init_coffin(void);

/**
 * [destroy_coffin description]
 * @param [name] [description]
 */
void destroy_coffin(coffin*);

/**
 * add_bone counts hi-c links into the hash map bones
 * @param  cf the coffin structure pointer
 * @param  n1 the index of the first seqid
 * @param  n2 the index of the second seqid
 * @param  p1 the position of the first seqid
 * @param  p2 the position of the second seqid
 * @param  l1 the length of the first sequence
 * @param  l2 the length of the second sequence
 * @return    0 upon success
 */
int add_bone(coffin * cf,
             uint32_t n1,
             uint32_t n2,
             uint32_t p1,
             uint32_t p2,
             uint32_t l1,
             uint32_t l2);

/**
 * Print coffin to flat txt
 * @param  cf pointer to a coffin
 * @return    0 upon success
 */
int print_coffin(coffin * cf);

/**
 * set the number of sequences
 * @param  cf pointer to a coffin
 * @param  uint32_t the number of sequences
 * @return          0 upon success
 */
int set_nseqs(coffin *, uint32_t);


/**
 * [add_seq description]
 * @param  cf       pointer to a coffin
 * @param  index    the # of the sequence in the header
 * @param  seq_name seq name [char pointer]
 * @param  name_l   the length of the char pointer [int]
 * @param  s_len    the sequence length in terms of bases [uint32_t]
 * @return          0 upon success, less than zero error
 */
int add_seq(coffin * cf,
  uint32_t index,
  char * seq_name,
  int name_l,
  uint32_t s_len);

/**
 * [bury description]
 * @param  cf     pointer to coffin
 * @param  fn_out file to write to
 * @return        0 upon success
 */
int bury(coffin * cf, FILE * fh);

/**
 * loads the data back in from a binary
 * @param  fn file name
 * @return    coffin * ; NULL if there is a problem
 */
coffin * exhume(const char * fn);

/**
 * Hashmap lookup and value returned. The two sequence ids (x,y) are sorted,
 * hashed and then a lookup is done. The int index determines which value you
 * are interested in, see bone structure for more information. If the index is
 * greater than 4 the code will segfault.
 *
 * @param  cf     pointer to coffin
 * @param  uint32_t index of sequence 1
 * @param  uint32_t index of sequence 2
 * @param  int      describes the direction you want
 * @return          [description]
 */
double get_value(coffin *, uint32_t, uint32_t, int);

#endif
