#ifndef QUEUE_H
#define QUEUE_H

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
typedef struct qNode{
  void * load;
  struct qNode * prev;
  struct qNode * next;
}qNode;

typedef struct Queue{
  qNode * root;
  uint32_t n  ;
  size_t element_size;
}Queue;


Queue * initQueue(size_t);
int destroyQueue(Queue * );
int pushQueue(Queue * , void *);
int popQueue(Queue *, void *);
int countQueue(Queue *);
#endif
