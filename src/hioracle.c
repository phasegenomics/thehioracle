#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <getopt.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include "gfa.h"
#include "bone.h"
#include "count_motif.h"
#include "khash.h"
#include "queue.h"


#define max(a,b) \
	({ __typeof__ (a)_a = (a); \
	   __typeof__ (b)_b = (b); \
	   _a > _b ? _a : _b; })


#define min(a,b) \
	({ __typeof__ (a)_a = (a); \
	   __typeof__ (b)_b = (b); \
	   _a < _b ? _a : _b; })

// khash is imported with bone.h
KHASH_MAP_INIT_INT(z, int);


struct link_info {
	uint64_t idx;
	uint64_t lookup;
	uint64_t link_id;
	double value;
};


// global options for the whole program;
struct options {
	char * fasta_fn;
	char * motifs;
	char * prefix;
	uint32_t nsteps;
	uint32_t niters;
} GLOBAL_OPTS;

struct DATA {
	struct link_info * links_values;
	uint32_t * consumed_vert;
	uint32_t * consumed_seg;
	uint8_t * consumed_in;
	uint8_t * consumed_out;
	uint32_t * hij;
	uint32_t center;
} DATA;

/*
 * This is the comparision function for qsort that does decending sort
 * @param  a void pointer to link_info
 * @param  b void pointer to link_info
 * @return   int for the qsort function
 */
int q_srt_cmp(const void * a, const void * b){

	double av = (*(struct link_info *)a).value;
	double bv = (*(struct link_info *)b).value;

	if(bv == av) return 0;
	if(av < bv ) return 1;
	return -1;
}

int loadQueue(gfa_t *g, uint8_t * seen, Queue * q, uint32_t v){

	gfa_arc_t * a;
	int na,i;

	a  = gfa_arc_a(g, v);
	na = gfa_arc_n(g, v);

	for(i = 0; i < na; i++) {
		if(seen[a[i].w>>1] == 1 || a[i].del == 1) continue;

		pushQueue(q, (void*)&a[i].w);
		seen[a[i].w>>1] = 1;

	}

	return 0;
}

int bbfs(gfa_t * g,
         coffin * co,
         gfa_arc_t l,
         uint32_t * lcount,
         uint32_t * rcount,
         uint32_t * lseg,
         uint32_t * rseg,
         uint32_t dist)
{

	uint32_t head = gfa_arc_head(l);
	uint32_t tail = gfa_arc_tail(l);

	head ^= 1;

	Queue * lq = initQueue(sizeof(uint32_t));
	Queue * rq = initQueue(sizeof(uint32_t));

	pushQueue(lq, (void *)&head);
	pushQueue(rq, (void *)&tail);

	uint8_t * seen = calloc(gfa_n_vtx(g), sizeof(uint8_t));

	seen[head>>1] = 1;
	seen[tail>>1] = 1;

	*lcount = 0;
	*rcount = 0;

	int rv, lv;


	while(countQueue(lq) || countQueue(rq)) {

		//fprintf(stderr, "HERE l:%i r:%i lc:%i rc:%i\n", queueCount(lq), queueCount(rq), *lcount, *rcount  );

		rv = popQueue(lq, (void *)&head);
		lv = popQueue(rq, (void *)&tail);

		//fprintf(stderr, "lv:%i rv:%i\n",rv, lv );

		if(lv && *lcount < dist) {
			lseg[*lcount] = head;
			*lcount = *lcount + 1;
			loadQueue(g, seen, lq, head);
		}

		if(rv && *rcount < dist) {
			rseg[*rcount] = tail;
			*rcount = *rcount + 1;
			loadQueue(g, seen, rq, tail);
		}
	}

	free(seen);
	destroyQueue(lq);
	destroyQueue(rq);

	return 0;

}

void load_link_values(gfa_t * g, coffin * co){

	DATA.links_values = malloc(sizeof(struct link_info) * g->n_arc);

	uint64_t new_idx, i;

	uint32_t lcount, rcount, l, r;

	uint32_t dist = 20;

	uint32_t * lsegs = calloc(dist, sizeof(uint32_t));
	uint32_t * rsegs = calloc(dist, sizeof(uint32_t));

	for(i = 0; i < g->n_arc; i++) {

		lcount = 0;
		rcount = 0;

		new_idx = g->arc[i].link_id;
		new_idx = (new_idx<<1) | g->arc[i].comp;

		DATA.links_values[new_idx].idx     = i;
		DATA.links_values[new_idx].lookup  = new_idx;
		DATA.links_values[new_idx].value   = 0;
		DATA.links_values[new_idx].link_id = g->arc[i].link_id;

		assert(DATA.links_values[new_idx].link_id == g->arc[i].link_id);

		bbfs(g, co, g->arc[i], &lcount, &rcount, lsegs, rsegs, dist);

		for(l = 0; l < lcount; l++) {
			for(r = 0; r < rcount; r++) {
				DATA.links_values[new_idx].value  += get_value(co, lsegs[l]>>1, rsegs[r]>>1, 4);
			}
		}

		/*
		   fprintf(stderr, "\nls: " );
		   for(l = 0; l < lcount; l++) {
		        fprintf(stderr, "%s,", g->seg[lsegs[l]>>1].name );
		   }
		   fprintf(stderr, " rs: " );
		   for(r = 0; r < rcount; r++) {
		        fprintf(stderr, "%s,", g->seg[rsegs[r]>>1].name );
		   }
		   fprintf(stderr, "\n" );
		 */
	}
	free(rsegs);
	free(lsegs);
}

/**
 * normalize_hic description: Does the normal normalization n/(c_1 + c_2) where n is the number of links between sequences i/j, and c_1/c_2 is the number of cutsites (any number of non-degenerate motifs allowed).
 * @param  co - coffin * co (see bone.h)
 * @param  so - details about this structure can be found in count_motif
 * @return    [description]
 */
int normalize_hic(coffin * co, struct sequenceInfo * so){

	khint_t k;
	bone * bh;

	uint32_t lk, rk, cut_count, i = 0;

	for (k = kh_begin(co->hashmap); k != kh_end(co->hashmap); ++k) {

		if(!kh_exist(co->hashmap, k)) continue;
		bh = &kh_value(co->hashmap, k);

		lk = lkey(bh->key);
		rk = rkey(bh->key);

		// This should never be hit, but better safe than sorry
		if(strcmp((char *)co->sinfo[lk].sname, (char*)so->dat[lk].name) != 0) {
			fprintf(stderr, "[%s]: %s != %s\n", __func__, (char*)co->sinfo[lk].sname, (char *)so->dat[lk].name );
			return -1;
		}

		// This should never be hit, but better safe than sorry
		if(strcmp((char *)co->sinfo[rk].sname, (char*)so->dat[rk].name) != 0) {
			fprintf(stderr, "[%s]: %s != %s\n", __func__, (char*)co->sinfo[lk].sname, (char*)so->dat[lk].name );
			return -1;
		}

		if( bh->counts[4] == 0 ) {
			fprintf(stderr, "[%s]: counts: %f %f %f %f %f\n", __func__, bh->counts[0],
			        bh->counts[1],
			        bh->counts[2],
			        bh->counts[3],
			        bh->counts[4] );
			return -2;
		}
		cut_count = 0;
		for(i = 0; i <= so->ncutters; i++) {
			cut_count += so->dat[lk].cutcount[i];
			cut_count += so->dat[rk].cutcount[i];
		}

		if(cut_count < 1) {
			bh->counts[4] = 0;
			continue;
		}

		bh->counts[4] = bh->counts[4] / (double)cut_count;

	}
	return 0;
}

/**
 * This function finds the best inter contig hi-c singal i<->j
 * @param  co coffin
 * @return a set of indicies. If there is no hi-c signal for contig i, the index == i. The memory needs to be cleaned up.
 */
uint32_t * best_mate(coffin * co, double ** vsum, gfa_t * g){

	fprintf(stderr, "INFO: finding highest hi-c pairs...\n");

	uint32_t * bm = malloc(sizeof(uint32_t)*co->n_seqs);
	double   * vv = malloc(sizeof(double)*co->n_seqs);

	*vsum = malloc(sizeof(double)*co->n_seqs);

	memset(vv, 0, sizeof(double)*co->n_seqs);

	if(bm == NULL) return bm;

	khint_t k;
	bone * bh;

	uint32_t i, j,  count = 0;

	for(i = 0; i < co->n_seqs; i++) {
		bm[i] = i;
		(*vsum)[i] = 0;
	}

	for (k = kh_begin(co->hashmap); k != kh_end(co->hashmap); ++k) {

		if(!kh_exist(co->hashmap,   k)) continue;
		bh = &kh_value(co->hashmap, k);

		i = lkey(bh->key);
		j = rkey(bh->key);


		count++;
		if((count % 1000000) == 0) fprintf(stderr, "INFO: finding highest hi-c pairs... [%i]\n", count);


		(*vsum)[i] += bh->counts[4];
		(*vsum)[j] += bh->counts[4];

		if(i == j) continue;

		if(vv[i] < bh->counts[4]) {
			vv[i] = bh->counts[4];
			bm[i] = j;
		}

		if(vv[j] < bh->counts[4]) {
			vv[j] = bh->counts[4];
			bm[j] = i;
		}

	}

	for(i = 0; i < co->n_seqs; i++) {

		uint32_t o = bm[i];

		if(bm[i] == i) continue;
		if(bm[o] != i) continue;

		//	fprintf(stderr, "INFO: highest pair link for i:%s is j:%s v:%f il:%i ij:%i\n", co->sinfo[i].sname, co->sinfo[bm[i]].sname, vv[i], co->sinfo[i].len, co->sinfo[bm[i]].len);
	}

	free(vv);
	return bm;
}



int validate_gfa_v_fasta(gfa_t * g, struct sequenceInfo * so){

	if(g->n_seg != so->nseq) {
		fprintf(stderr, "WARNING: GFA does NOT match FASTA : n_segments != n_seqs\n");
		return -1;
	}
	uint32_t s = 0;
	for(; s < g->n_seg; s++) {
		if (strcmp( (char*)so->dat[s].name, g->seg[s].name) != 0) {
			fprintf(stderr, "WARNING: GFA does NOT match FASTA : segment name != sequence name\n");
			fprintf(stderr, "seq1:%s seq2:%s\n", so->dat[s].name, g->seg[s].name);
			return -2;
		}
	}

	return 0;
}
/**
 * Validate_gfa_v_coffin description: checks that the coffin sequence names match the gfa sequence names in order
 * @param  g - gfa_t *t
 * @param  c - coffin * co
 * @return  zero upon success and negative one if failure
 */
int validate_gfa_v_coffin(gfa_t * g, coffin * c)
{
	if(g->n_seg != c->n_seqs) {
		fprintf(stderr, "WARNING: GFA does NOT match Hi-C data : n_segments != n_seqs\n");

		return -1;
	}

	uint32_t s = 0;
	for(; s < g->n_seg; s++) {
		if (strcmp( (char*)c->sinfo[s].sname, g->seg[s].name) != 0) {
			fprintf(stderr, "WARNING: GFA does NOT match Hi-C data : segment name != sequence name\n");
			fprintf(stderr, "seq1:%s seq2:%s\n", c->sinfo[s].sname, g->seg[s].name);
			return -2;
		}
	}
	fprintf(stderr, "OH HEY, the GFA matches the coffin (Hi-C data) : ʕ•̫͡•ʕ•̫͡•ʔ•̫͡•ʔ\n" );
	return 0;
}

void print_usage(void){
	fprintf(stderr, "\nusage: hioracle -p prefix -m GATC -f your.fasta <your.coffin> <your.gfa> 2> out.err \n\n");
}
/**
 * del_all_links : set all the links in the graph to deleted
 * @param g - gfa_t * to the graph
 */
void del_all_links(gfa_t * g){
	uint64_t i = 0;
	for(; i < g->n_arc; i++) {
		g->arc[i].del = 1;
	}
}
/**
 * flip_radom_link description : flips the deletion state of a link
 * @param g - gfa_t * to the graph
 */
void flip_radom_link(gfa_t * g){
	uint64_t r = rand() % g->n_arc;
	g->arc[r].del ^= 1;
}

/**
 * link_down_by_highest description : Sorts the edges by there weight then goes down the list un-deleting edges. A node can only have one incomming and outgoing edge.
 * @param g - gfa_t * to the graph
 */
void link_down_by_highest(gfa_t * g){

	del_all_links(g);

	qsort(DATA.links_values, g->n_arc, sizeof(struct link_info), q_srt_cmp);

	DATA.consumed_in  = calloc(g->n_seg<<1, sizeof(uint8_t));
	DATA.consumed_out = calloc(g->n_seg<<1, sizeof(uint8_t));

	uint64_t i,idx,ngtz,nglk;
	ngtz = 0; nglk = 0;

	uint32_t head_seg, tail_seg = 0;
	for(i = 0; i < g->n_arc; i++) {
		if(DATA.links_values[i].value == 0) break;
		idx = DATA.links_values[i].idx;
		ngtz++;
		head_seg = gfa_arc_head(g->arc[idx]);
		tail_seg = g->arc[idx].w;

		if( DATA.consumed_in[head_seg] || DATA.consumed_in[ tail_seg] || g->arc[idx].comp ) continue;

		g->arc[idx].del = 0;

		nglk++;

		DATA.consumed_in[head_seg] = 1;
		DATA.consumed_in[tail_seg] = 1;

	}

	fprintf(stderr, "INFO: There are %llu links with hi-c weight > 0\n", ngtz );
	fprintf(stderr, "INFO: There are  %llu links set\n", nglk );

	free(DATA.consumed_in );
	free(DATA.consumed_out);
}

/**
 * print_weighted_sorted_links description: prints the sorted link weights
 * @param g  - gfa_t *
 * @param fp - a FILE *
 */
void print_weighted_sorted_links(gfa_t * g, FILE * fp){

	uint32_t head_seg, tail_seg;

	uint64_t i, idx;

	for(i = 0; i < g->n_arc; i++) {
		idx = DATA.links_values[i].idx;

		assert(g->arc[idx].link_id == DATA.links_values[i].link_id);

		head_seg = gfa_arc_head(g->arc[idx])>>1;
		tail_seg = g->arc[idx].w>>1;

		if(g->arc[idx].comp) continue;

		fprintf(fp, "%s -- %s : %llu : del = %i : %f\n", g->seg[head_seg].name, g->seg[tail_seg].name, g->arc[idx].link_id, g->arc[idx].del, DATA.links_values[i].value );
	}
}

int main(int argc, char *argv[])
{
	int c;
	const char    * short_opt = "hf:m:p:";
	struct option long_opt[] =
	{
		{"help",          no_argument,       NULL, 'h'},
		{"fasta",         required_argument, NULL, 'f'},
		{"motifs",        required_argument, NULL, 'm'},
		{"prefix",        required_argument, NULL, 'p'},
		{NULL,            0,                 NULL, 0  }

	};

	while((c = getopt_long(argc, argv, short_opt, long_opt, NULL)) != -1)
	{
		switch(c)
		{
		case 'h':
		{
			print_usage();
			exit(1);
		}
		case 'f':
		{
			GLOBAL_OPTS.fasta_fn = optarg;
			break;
		}
		case 'p':
		{
			GLOBAL_OPTS.prefix = optarg;
			break;
		}
		case 'm':
		{
			GLOBAL_OPTS.motifs = optarg;
			break;
		}

		default:
		{
			/* invalid option */
			fprintf(stderr, "%s: option '-%c' is invalid: ignored\n",
			        argv[0], optopt);
			break;
		}
		}
	}

	if(GLOBAL_OPTS.prefix == NULL) {
		print_usage();
		return 1;
	}

	if(argc < 3) {
		print_usage();
		return 1;
	}

	srand(time(NULL));

	gfa_t  *  g;
	coffin * co;


	fprintf(stderr, "Reading coffin: %s\n", argv[argc - 2] );

	co = exhume(argv[argc - 2]);
	if(co == NULL) {
		return 2;
	}

	fprintf(stderr, "Reading GFA: %s\n", argv[argc - 1] );

	g = gfa_read(argv[argc - 1]);
	if (g == 0) {
		fprintf(stderr, "ERROR: failed to read the graph\n");
		return 2;
	}

	if(validate_gfa_v_coffin(g, co) < 0) {
		gfa_destroy(g);
		destroy_coffin(co);
		return 1;
	}

	struct sequenceInfo * sInfo = load_seq_info(GLOBAL_OPTS.fasta_fn, GLOBAL_OPTS.motifs);

	char * cutsite_fn = malloc(strlen(GLOBAL_OPTS.prefix) + 12);
	char * results_fn = malloc(strlen(GLOBAL_OPTS.prefix) + 12);
	char * links_fn   = malloc(strlen(GLOBAL_OPTS.prefix) + 12);

	strcat(cutsite_fn, GLOBAL_OPTS.prefix); strcat(cutsite_fn, ".seqs.txt");
	strcat(results_fn, GLOBAL_OPTS.prefix); strcat(results_fn, ".gfa");
	strcat(links_fn,   GLOBAL_OPTS.prefix); strcat(links_fn, ".links.txt");

	FILE * cutsites; FILE * results; FILE * links;

	cutsites = fopen(cutsite_fn, "w");
	results  = fopen(results_fn, "w");
	links    = fopen(links_fn,   "w");

	print_sequenceInfo(cutsites, sInfo);

	if(validate_gfa_v_fasta(g, sInfo) < 0) {
		fprintf(stderr, "FATAL: gfa<->fasta validation failed\n");
		return 1;
	}

	if(normalize_hic(co, sInfo) < 0) {
		fprintf(stderr, "FATAL: hi-c validation failed\n");
		return 1;
	}

	fprintf(stderr, "INFO: n_arcs: %llu n_seg: %i n_vert: %i\n", g->n_arc, g->n_seg, gfa_n_vtx(g) );

	load_link_values(g, co);
	link_down_by_highest(g);
	print_weighted_sorted_links(g, links);
	gfa_print(g, results, 0);

	gfa_destroy(g);
	destroy_coffin(co);
	destroy_sequence_info(sInfo);

	fclose(cutsites);
	fclose(results);
	fclose(links);

	free(links_fn);
	free(cutsite_fn);
	free(results_fn);

	fprintf(stderr, "OH HEY, the hioracle finished normally: ʕ•ᴥ•ʔ\n");

	return 0;
}
