#include "bone.h"
#include <assert.h>

/*
   This function allocates the memory for the coffin
 */
coffin * init_coffin(void)
{
	coffin * links;
	links = (coffin *) malloc(sizeof(coffin));

	links->n_seqs      = 0;
	links->n_keys      = 0;

	links->hashmap = kh_init(s);
	return links;
}

/*
   This function destroys the coffin
 */
void destroy_coffin(coffin * cf)
{

	if(cf->n_seqs != 0) {
		int i = 0;
		for(i = 0; i < cf->n_seqs; i++) {
			free(cf->sinfo[i].sname);
		}
	}

	kh_destroy(s, cf->hashmap);
	free(cf);
}

/*
   This function counts into bones
 */
int add_bone(coffin * cf,
             uint32_t n1,
             uint32_t n2,
             uint32_t p1,
             uint32_t p2,
             uint32_t l1,
             uint32_t l2)
{

	int code;
	int direction = 0;

	// See bone.h for more details
	if(p1 > (l1 / 2) ) direction = 2;
	if(p2 > (l2 / 2) ) direction = direction | 1;

	// Key generated in header macro
	uint64_t key = makekey(n1, n2);

	/*
	   fprintf(stderr, "%" PRId64 "\t", key);
	   fprintf(stderr, "%" PRId64 "\t", lkey(key));
	   fprintf(stderr, "%" PRId64 "\t", rkey(key));
	   fprintf(stderr, "%" PRId64 "\t", n1 );
	   fprintf(stderr, "%" PRId64 "\n", n2 );
	 */

	// Query to see if the key is in the hash
	khint_t k = kh_get(s, cf->hashmap, key);
	// The key was not found so we need to insert it

	if(k == kh_end(cf->hashmap)) {
		k = kh_put(s, cf->hashmap, key, &code);

		bone * bh = &kh_value(cf->hashmap, k);

		bh->counts[0] = 0;
		bh->counts[1] = 0;
		bh->counts[2] = 0;
		bh->counts[3] = 0;

		bh->counts[direction] += 1;
		bh->counts[4] = 1;

		bh->key = key;

	}
	// The key was found and we are now loading it up.
	else{

		kh_value(cf->hashmap, k).counts[direction] += 1;
		kh_value(cf->hashmap, k).counts[4] += 1;
		//  assert(kh_value(cf->hashmap, k).counts[4] != 0);
	}

	return 0;
}

void __printSeqNames(seq_info * sInfo){

	uint32_t i = 0;

	for(; i < sInfo->namelen - 1; i++) {
		printf("%c", (char)sInfo->sname[i]);
	}

}

int print_coffin(coffin * cf)
{
	khint_t k;
	bone * bh;
	for (k = kh_begin(cf->hashmap); k != kh_end(cf->hashmap); ++k) {
		bh = &kh_value(cf->hashmap, k);

		if(!kh_exist(cf->hashmap, k)) continue;


		if(cf->n_seqs != 0) {
			__printSeqNames(&cf->sinfo[lkey(bh->key)]);
			fprintf(stdout, "\t");
			__printSeqNames(&cf->sinfo[rkey(bh->key)]);
			fprintf(stdout, "\t");
		}


		fprintf(stdout, "%" PRId64 "\t", bh->key);
		fprintf(stdout, "%f\t", bh->counts[0] );
		fprintf(stdout, "%f\t", bh->counts[1] );
		fprintf(stdout, "%f\t", bh->counts[2] );
		fprintf(stdout, "%f\t", bh->counts[3] );
		fprintf(stdout, "%f\n", bh->counts[4] );
	}
	return 0;
}

int set_nseqs(coffin * cf, uint32_t n){
	cf->n_seqs = n;
	cf->sinfo = malloc(n * sizeof(seq_info));
	return 0;
}

int add_seq(coffin * cf,
            uint32_t index,
            char * seq_name,
            int name_l,
            uint32_t s_len)
{
	if(cf->n_seqs == 0) return -1;

	cf->sinfo[index].sname = malloc(sizeof(uint8_t)* name_l);

	if(cf->sinfo[index].sname == NULL) return -1;

	int i = 0;

	for(; i < name_l; i++) {
		cf->sinfo[index].sname[i] = (uint8_t)seq_name[i];
	}
	cf->sinfo[index].namelen = (uint32_t)name_l;
	cf->sinfo[index].len  = s_len;

	return 0;
}

int bury(coffin * cf, FILE * fh)
{
	fprintf(stderr, "INFO: burrying coffin\n");

	double s = sizeof(double);
	double spacer = 1984;


	fwrite(&s, sizeof(double), 1, fh);
	fwrite(&cf->n_seqs, sizeof(uint32_t), 1, fh);

	uint32_t i = 0;
	for(; i < cf->n_seqs; i++) {
		fwrite(&cf->sinfo[i].namelen, sizeof(uint32_t), 1, fh );
		fwrite(&cf->sinfo[i].len,   sizeof(uint32_t), 1, fh );
		fwrite(cf->sinfo[i].sname,  sizeof(uint8_t), cf->sinfo[i].namelen, fh );
	}
	fwrite(&spacer, sizeof(double), 1, fh);

	khint_t k;
	bone * bh;

	uint32_t counter = 0;
	for (k = kh_begin(cf->hashmap); k != kh_end(cf->hashmap); ++k) {

		if(!kh_exist(cf->hashmap, k)) continue;
		counter += 1;
	}
	fwrite(&counter, sizeof(uint32_t), 1, fh);


	for (k = kh_begin(cf->hashmap); k != kh_end(cf->hashmap); ++k) {
		bh = &kh_value(cf->hashmap, k);

		if(!kh_exist(cf->hashmap, k)) continue;

		fwrite(&bh->key, sizeof(uint64_t), 1, fh );
		fwrite(bh->counts, sizeof(double), 5, fh );
	}


	fwrite(&spacer, sizeof(double), 1, fh);

	fprintf(stderr, "INFO: done burrying coffin\n");

	return 0;

}

coffin * exhume(const char * fn)
{
	int code;
	coffin * cf = init_coffin();

	if( access( fn, F_OK ) != -1 ) {
		// file exists
	} else {
		fprintf(stderr, "ERROR: file: [%s] does not exist\n", fn );
		code = 1;
		goto fail;
	}

	FILE * fh;
	fh = fopen(fn, "rb");



	double tmp = 0;

	fread(&tmp, sizeof(double), 1, fh);

	if(tmp != sizeof(double)) {
		code = 10;
		goto fail;
	}

	fread(&cf->n_seqs, sizeof(uint32_t), 1, fh);

	cf->sinfo = malloc(cf->n_seqs * sizeof(seq_info));

	uint32_t i = 0;
	for(; i < cf->n_seqs; i++) {
		fread(&cf->sinfo[i].namelen, sizeof(uint32_t), 1, fh );
		fread(&cf->sinfo[i].len, sizeof(uint32_t), 1, fh );

		cf->sinfo[i].sname = malloc(sizeof(uint8_t) * cf->sinfo[i].namelen );

		fread(cf->sinfo[i].sname, sizeof(uint8_t),cf->sinfo[i].namelen, fh );
	}

	fread(&tmp, sizeof(double), 1, fh);
	if(tmp != 1984) {
		code = 12;
		goto fail;
	}

	fread(&cf->n_keys, sizeof(uint32_t), 1, fh);

	uint64_t key;
	double vals[5];
	khint_t k;


	for(i = 0; i < cf->n_keys; i++) {
		fread(&key, sizeof(uint64_t), 1, fh );
		fread(vals, sizeof(vals), 1, fh );
		k = kh_put(s, cf->hashmap, key, &code);
		if(code < 0) goto fail;
		bone * bh = &kh_value(cf->hashmap, k);

		bh->key       = key;
		bh->counts[0] = vals[0];
		bh->counts[1] = vals[1];
		bh->counts[2] = vals[2];
		bh->counts[3] = vals[3];
		bh->counts[4] = vals[4];


	}

	fread(&tmp, sizeof(double), 1, fh);

	if(tmp != 1984) {
		code = 11;
		goto fail;
	}
	return cf;

fail:
	fprintf(stderr, "ERROR: coffin read err: %i\n", code);
	destroy_coffin(cf);
	cf = NULL;
	return cf;

}

double get_value(coffin * cf, uint32_t x, uint32_t y, int idx){
	uint64_t key = makekey(x, y);
	khint_t k = kh_get(s, cf->hashmap, key);
	if(k == kh_end(cf->hashmap)) return 0;
	return kh_value(cf->hashmap, k).counts[idx];
}
