#include "queue.h"

Queue * initQueue(size_t element_size){

	Queue * q = malloc(sizeof(Queue));

	q->element_size = element_size;
	q->root = NULL;
	q->n = 0;

}

int _destroyqNode(qNode * qn){
	free(qn->load);
	free(qn);
	return 0;
}

int _destroyAllqNode(qNode * qn){

	if(qn == NULL) return 1;

	qNode * tmp = qn->next;
	_destroyqNode(qn);

	return _destroyqNode(tmp);

}

int destroyQueue(Queue * q){
	_destroyqNode(q->root);
	free(q);
	return 0;
}
int pushQueue(Queue * q, void * d){

	qNode * tmp = q->root;
	while(tmp->next != NULL) {
		tmp = tmp->next;
	}

	qNode * insert = malloc(sizeof(qNode));
	insert->load    = malloc(sizeof(q->element_size));
	memcpy(insert->load, d, q->element_size);

	tmp->next = insert;
	insert->prev = tmp;

	q->n++;
	return 0;
}
int popQueue(Queue * q, void *d){
	if(q->root == NULL) return 0;

	memcpy(d, q->root->load, q->element_size);

	qNode * tmp = q->root;

	q->root = q->root->next;
	q->root->prev = NULL;

	_destroyqNode(tmp);

	q->n--;

	return 1;
}

int countQueue(Queue * q){
	return q->n;
}
